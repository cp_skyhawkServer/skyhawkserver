#include <AltSoftSerial.h>
//for ARDUINO UNO RX = PORT 8, TX = PORT 9, PWR = PORT 10 ***STRICT PORT***
//(4800,9600,19200,38400,57600,115200),(300,600,1200,2400,4800,9600,19200,38400,57600,115200,230400,460800,921600,3200000,3686400,4000000)
AltSoftSerial mySerial(51,50);

void setup()
{
  Serial.begin(115200);
  while (!Serial);
  Serial.println("AT Command to 3G-UC15");
  mySerial.begin(9600);
//  These lines are used for open UC15 by sending pulse
  pinMode(44,OUTPUT);
  digitalWrite(44,HIGH);
  delay(3000);
  digitalWrite(44,LOW);
  
}

void loop() {
//  String str;
  if (mySerial.available()) {
    Serial.write(mySerial.read());
  }
  if (Serial.available()) {
    mySerial.write(Serial.read());
//    Serial.read();
  }
}
