#define HIGH_FREQ_INTERVAL 250
#define LOW_FREQ_INTERVAL 1000
float Interval[90];
int i = 0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  int index = 0, initInterval = 2000;
  pinMode(26,OUTPUT);
  for(int counter = 0; counter < 30; counter++){
    for(index = 0; index < 3; index++){
      Interval[counter * 3 + index] = initInterval;
    } 
    initInterval*=0.8;
  }
}

void loop() {
  if(i > 89) return;
  // put your main code here, to run repeatedly:
  for(int t = 0; t<100;t++){
    digitalWrite(26,HIGH); 
    delayMicroseconds(HIGH_FREQ_INTERVAL);
    digitalWrite(26,LOW); 
    delayMicroseconds(HIGH_FREQ_INTERVAL);
  }
//  for(int t = 0; t<1000;t++){
//    digitalWrite(26,HIGH); 
//    delayMicroseconds(LOW_FREQ_INTERVAL);
//    digitalWrite(26,LOW); 
//    delayMicroseconds(LOW_FREQ_INTERVAL);
//  }
//  a = Interval
  Serial.println(Interval[i]);
  delay(Interval[i]);
  i+=1;
}
