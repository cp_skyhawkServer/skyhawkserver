unsigned int counter=0;

void docount()  // counts from the speed sensor
{
  counter++;  // increase +1 the counter value
  Serial.println(counter);
} 

void setup() 
{
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(19), docount, RISING);  // increase counter when speed sensor pin goes High
} 

void loop()
{
  
}
