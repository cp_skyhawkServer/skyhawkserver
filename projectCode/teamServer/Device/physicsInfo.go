package Device

import (
	"time"

	"../common"
	"github.com/gin-gonic/gin"
	"github.com/influxdata/influxdb/client/v2"
)

// LogPhysicsInfo : Log device info into influxDB
func LogPhysicsInfo(jsonData gin.H) {
	influxConn := common.ConnectInfluxDB()
	influxTable, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  "SkyhawkPhase1",
		Precision: "s",
	})
	newRecord, err := client.NewPoint("physicalLog", map[string]string{
		"flightID": jsonData["payload"].(map[string]interface{})["flightID"].(string),
	}, map[string]interface{}{
		"aX":           jsonData["payload"].(map[string]interface{})["aX"].(float64) * 9.80665 / 16384,
		"aY":           jsonData["payload"].(map[string]interface{})["aY"].(float64) * 9.80665 / 16384,
		"aZ":           jsonData["payload"].(map[string]interface{})["aZ"].(float64) * 9.80665 / 16384,
		"angleX":       jsonData["payload"].(map[string]interface{})["angleX"],
		"angleY":       jsonData["payload"].(map[string]interface{})["angleY"].(float64) * (-1),
		"angleZ":       jsonData["payload"].(map[string]interface{})["angleZ"].(float64) * (-1),
		"gyroX":        jsonData["payload"].(map[string]interface{})["gyroX"],
		"gyroY":        jsonData["payload"].(map[string]interface{})["gyroY"],
		"gyroZ":        jsonData["payload"].(map[string]interface{})["gyroZ"],
		"lat":          jsonData["payload"].(map[string]interface{})["lat"],
		"long":         jsonData["payload"].(map[string]interface{})["long"],
		"devTimestamp": jsonData["payload"].(map[string]interface{})["timestamp"],
	}, time.Now())
	common.CheckErr(err)
	influxTable.AddPoint(newRecord)
	common.CheckErr(influxConn.Write(influxTable))
	common.CheckErr(influxConn.Close())
	common.BroadCastAll("FetchDeviceStatus", "")
}
