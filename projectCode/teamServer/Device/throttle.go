package Device

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/surgemq/message"

	"../common"
)

// SetThrottle : Set Device motor throttle power
func SetThrottle(inputValue string) error {
	throttleValue := calculateThrottle(inputValue)
	if throttleValue < 0 {
		return errors.New("throttleValue should be integer and between 0 and 100")
	}
	throttleJSON, _ := json.Marshal(common.ServerToDeviceMsg{
		Command:   "throttle",
		Payload:   throttleValue,
		Timestamp: time.Now().UnixNano() / int64(time.Second),
	})
	publishMsg := message.NewPublishMessage()
	publishMsg.SetTopic([]byte("skyhawkPhase1"))
	publishMsg.SetPayload([]byte(string(throttleJSON)))
	publishMsg.SetQoS(2)
	return common.MqttListener.Publish(publishMsg, nil)
}

func calculateThrottle(inputValue string) int {
	rawValue, err := strconv.Atoi(inputValue)
	if err != nil {
		rawValue = -1
	}
	outputValue := 0
	if rawValue < 0 || rawValue > 100 {
		outputValue = -1
	} else if rawValue == 0 {
		outputValue = 700
	} else {
		outputValue = rawValue*(2300-1550)/100 + 1550
	}
	return outputValue
}
