package Device

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/surgemq/message"

	"../common"
)

// SetCamera : Set Camera angle
func SetCamera(direction string, inputValue string) error {
	angleValue := calculateAngle(inputValue)
	if angleValue == -1 {
		return errors.New("angle should be integer and between 0 and 90")
	}

	camJSON, _ := json.Marshal(common.ServerToDeviceMsg{
		Command:   "Cam_" + direction,
		Payload:   angleValue,
		Timestamp: time.Now().UnixNano() / int64(time.Second),
	})
	publishMsg := message.NewPublishMessage()
	publishMsg.SetTopic([]byte("skyhawkPhase1"))
	publishMsg.SetPayload([]byte(string(camJSON)))
	publishMsg.SetQoS(2)
	return common.MqttListener.Publish(publishMsg, nil)
}

func calculateAngle(inputValue string) int {
	rawValue, err := strconv.Atoi(inputValue)
	if err != nil {
		rawValue = -1
	}
	outputValue := 0
	if rawValue < 0 || rawValue > 90 {
		outputValue = -1
	} else {
		outputValue = rawValue
	}
	return outputValue
}
