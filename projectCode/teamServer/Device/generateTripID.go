package Device

import (
	"encoding/json"
	"fmt"
	"time"

	"../common"
	"github.com/gin-gonic/gin"
	"github.com/surgemq/message"
)

// GenerateTripID : Generate new trip ID for new trip
func GenerateTripID(jsonData gin.H) {
	var flightID string
	influxConn := common.ConnectInfluxDB()
	for {
		flightID = common.RandString(13)
		queryResult, err := common.QueryInfluxDB(influxConn, fmt.Sprintf("SELECT * FROM physicalLog WHERE flightID = '%s'", flightID))
		common.CheckErr(err)
		if len(queryResult[0].Series) == 0 {
			break
		}
	}
	newTripIDDeclaration, _ := json.Marshal(common.ServerToDeviceMsg{
		Command:   "newTripID",
		Payload:   flightID,
		Timestamp: time.Now().UnixNano() / int64(time.Second),
	})
	publishMsg := message.NewPublishMessage()
	publishMsg.SetTopic([]byte("skyhawkPhase1"))
	publishMsg.SetPayload([]byte(string(newTripIDDeclaration)))
	publishMsg.SetQoS(2)
	common.CheckErr(common.MqttListener.Publish(publishMsg, nil))
	common.CheckErr(influxConn.Close())
}
