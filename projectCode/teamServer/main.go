package main

import (
	"regexp"

	"./router"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

var mustAuthenBeforeUseAPI = []string{
	"/ws",
	"/api/v1/deviceLog/physical/.*",
}

func routeNotFound(c *gin.Context) {
	c.JSON(404, gin.H{
		"err": "404 Not Found",
	})
}

func middleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		for _, APIPATH := range mustAuthenBeforeUseAPI {

			authenAPI, _ := regexp.Compile(APIPATH)
			if authenAPI.MatchString(c.Request.RequestURI) {
				println("True")
				currentPermission, isAuthenticated := session.Get("Permission").(int)

				if isAuthenticated && currentPermission == 1 {
					c.Next()
					return
				}

				c.AbortWithStatus(403)
			}
		}
		c.Next()
	}
}

func main() {
	// MQTT incoming handle init
	go router.MQTT()
	// Construct HTTP server object, 404 implementation and user session
	r := gin.Default()
	r.NoRoute(routeNotFound)
	store := sessions.NewCookieStore([]byte("b715e81f0060b6c53a56c434b2f0e7d7f6f73cb4f0a05db538ef32cad4d7abb5"))
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowCredentials = true
	r.Use(static.Serve("/", static.LocalFile("./static", true)), sessions.Sessions("skyhawkServer", store), middleware(), cors.New(config))

	// HTTP WebSocket Router
	r.GET("/ws", router.WebSocket)

	// HTTP API Router
	router.HTTPAPI(r)

	//Start Server!
	r.Run(":8080")
}
