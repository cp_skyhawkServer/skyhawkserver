function loginStatus(callbackWhenLoggedIn, callbackWhenNotLoggedIn) {
  $.ajax({
    url: "/api/v1/authStatus",
    type: 'GET',
    dataType: 'JSON',
    success: function(response){
        if(response.success){
          callbackWhenLoggedIn(response)
        }
        else {
          callbackWhenNotLoggedIn(response)
        }
    }
  });
}
function logout() {
  $.ajax({
    url: "/api/v1/logout",
    type: 'GET',
    dataType: 'JSON',
    success: function(response){
      window.location.href = "/"
    },
    error: function (error) {
      alert(error)
    }
  });
}
