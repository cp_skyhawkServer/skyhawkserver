function getProfile() {
    $.ajax({
        url: "/api/v1/UserProfile",
        type: 'GET',
        dataType: 'JSON',
        success: function(response){
            $("#vname").val(response.result.Name) ;
            $("#vsurname").val(response.result.Surname) ;
            $("#vbday").val(response.result.Birthday) ;
            $("#vage").val(response.result.Age) ;
            $("#voppertunity").val(response.result.Oppertunity) ;
            $("#vtel").val(response.result.Tel) ;
            $("#vemail").val(response.result.Email) ;
            if(response.result.Sex == true) {
                $("input[name='vsex'][value='true']").prop('checked', true);
            } else {
                $("input[name='vsex'][value='false']").prop('checked', true);
            }
        }
    });
}

function changeProfile() {
    var modifyProfile = {
        "Name": $("#vname").val(),
        "Surname": $("#vsurname").val(),
        "Birthday": $("#vbday").val(),
        "Age": $("#vage").val(),
        "Sex": $("input[name='vsex']:checked").val() ,
        "Oppertunity": $("#voppertunity").val(),
        "Tel": $("#vtel").val() ,
        "Email":  $("#vemail").val()
    }
    $.ajax({
        url: "/api/v1/UserProfile",
        type: 'POST',
        dataType: 'JSON',
        data: {
            modifyJSON : modifyProfile
        },
        success: function(){
            alert("success");
        }
    });
}
