var wsConnector = new WebSocket(`ws://${window.location.hostname}:8080/ws`)
var currentThrottle = 0

function onWebSocketReceiveMessage(param) {
  console.log(param.data)
  window.debug2 = param
}
function setupWebSocket() {
  wsConnector.onerror = function (error) {
    console.error(error);
    wsConnector = new WebSocket(`ws://${window.location.hostname}:8080/ws`)
  }
  wsConnector.onmessage = onWebSocketReceiveMessage
}

function increaseThrottle() {
  currentThrottle = currentThrottle < 100 ? currentThrottle + 1 : 100
  setThrottle(currentThrottle)
}
function decreaseThrottle() {
  currentThrottle = currentThrottle > 0 ? currentThrottle - 1 : 0
  setThrottle(currentThrottle)
}

function setThrottle(throttle) {
  wsConnector.send(JSON.stringify({
    Event: "throttle",
    Payload: throttle.toString()
  }))
}
