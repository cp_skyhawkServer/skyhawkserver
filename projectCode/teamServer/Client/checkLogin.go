package Client

import (
	"../common"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

//User : User struct for login
type User struct {
	Username string
	Password string
}

// CheckLogin : Simple login by set user session try with username "poomrokc" and password "poomrokc" for testing purpose
func CheckLogin(c *gin.Context) {
	session := sessions.Default(c)
	success := false
	username := ""
	errmsg := "Username not found"
	MongoSession := common.ConnectMDB()
	defer MongoSession.Close()
	UserCollection := MongoSession.DB("SkyhawkPhase1").C("WebUsers")
	queryUser := bson.M{"username": c.PostForm("username")}
	result := User{}
	err := UserCollection.Find(queryUser).One(&result)
	if !common.CheckErr(err) {
		if result.Password == common.SHA256(c.PostForm("password")) {
			success = true
			username = c.PostForm("username")
			errmsg = ""
			session.Set("username", username)
			session.Set("Permission", 1)
		} else {
			errmsg = "Wrong password"
		}
	}
	session.Save()
	c.JSON(200, gin.H{
		"success":  success,
		"errmsg":   errmsg,
		"username": username,
	})
}

//AuthStatus return success=true with authentication info, or success=false if not authenticated
func AuthStatus(c *gin.Context) {

	success := false
	username := ""

	session := sessions.Default(c)

	currentPermission, isAuthenticated := session.Get("Permission").(int)

	if isAuthenticated && currentPermission == 1 {
		success = true
		username = session.Get("username").(string)
	}

	c.JSON(200, gin.H{
		"success":  success,
		"username": username,
	})
}

// Logout : Logout from system
func Logout(c *gin.Context) {
	session := sessions.Default(c)
	session.Set("username", "")
	session.Set("Permission", 0)
	session.Save()
	c.JSON(200, gin.H{
		"success": true,
	})
}
