package Client

import (
	"../common"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

type profile struct {
	Name        string
	Surname     string
	Birthday    string `bson:"Birthday"`
	Age         int32
	Sex         bool
	Oppertunity string
	Tel         string `bson:"Tel"`
	Email       string `bson:"e-mail"`
}

// UserProfile : Get current user profile
func UserProfile(c *gin.Context) {
	session := sessions.Default(c)
	MongoSession := common.ConnectMDB()
	defer MongoSession.Close()
	UserCollection := MongoSession.DB("SkyhawkPhase1").C("WebUsers")
	queryUser := bson.M{"username": session.Get("username")}
	result := profile{}
	err := UserCollection.Find(queryUser).One(&result)
	c.JSON(200, gin.H{
		"success": !common.CheckErr(err),
		"errmsg":  err,
		"result":  result,
	})
}

// UserUpdateProfile : Get current user profile
func UserUpdateProfile(c *gin.Context) {
	session := sessions.Default(c)
	MongoSession := common.ConnectMDB()
	defer MongoSession.Close()
	UserCollection := MongoSession.DB("SkyhawkPhase1").C("WebUsers")
	queryUser := bson.M{"username": session.Get("username")}
	jsonupdate := c.Param("UpdateSyntax")
	profile := common.RequestFormatter([]byte(jsonupdate))
	err := UserCollection.Update(queryUser, profile)
	c.JSON(200, gin.H{
		"err": err,
	})
}
