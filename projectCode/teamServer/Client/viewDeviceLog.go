package Client

import (
	"encoding/json"
	"fmt"
	"time"

	"../common"
	"github.com/gin-gonic/gin"
)

var outputFieldName = map[string]string{
	"flightID":     "flightID",
	"aX":           "ความเร่งเชิงเส้น X",
	"aY":           "ความเร่งเชิงเส้น Y",
	"aZ":           "ความเร่งเชิงเส้น Z",
	"angleX":       "pitch",
	"angleY":       "roll",
	"angleZ":       "azimuth",
	"gyroX":        "ความเร่งเชิงมุม X",
	"gyroY":        "ความเร่งเชิงมุม Y",
	"gyroZ":        "ความเร่งเชิงมุม Z",
	"lat":          "latitude",
	"long":         "longtitude",
	"time":         "timestamp",
	"devTimestamp": "เวลาส่งของอุปกรณ์",
}

// ViewDevicePhysicalLog : Query physics log that store in mongoDB and return to client as JSON
func ViewDevicePhysicalLog(c *gin.Context) {
	jsonData := []gin.H{}
	influxDB := common.ConnectInfluxDB()
	queryResult, err := common.QueryInfluxDB(influxDB, fmt.Sprintf("SELECT * FROM physicalLog WHERE flightID = '%s'", c.Param("flightID")))
	if common.CheckErr(err) {
		c.JSON(500, gin.H{
			"result": err.Error(),
		})
		return
	}
	if len(queryResult[0].Series) == 0 {
		c.JSON(200, gin.H{
			"result": []gin.H{},
		})
		return
	}
	for _, row := range queryResult[0].Series[0].Values {
		tempRow := gin.H{}
		for index, columnName := range queryResult[0].Series[0].Columns {
			tempRow[columnName] = row[index]
		}
		jsonData = append(jsonData, tempRow)
	}
	c.JSON(200, gin.H{
		"result": jsonData,
	})
}

// EarliestDevicePhysicalLog : Query Earliest physics log that store in mongoDB and return as JSON string
func EarliestDevicePhysicalLog(flightID string) string {
	jsonData := make(map[string]map[string]interface{})
	jsonData["status"] = make(map[string]interface{})

	influxDB := common.ConnectInfluxDB()
	queryResult, err := common.QueryInfluxDB(influxDB, fmt.Sprintf("SELECT * FROM physicalLog WHERE flightID = '%s' ORDER BY time DESC LIMIT 1", flightID))
	if common.CheckErr(err) {
		jsonData["status"]["success"] = false
		jsonData["status"]["errcode"] = "500" //internal error
		jsonString, _ := json.Marshal(jsonData)
		return fmt.Sprintf("%s", jsonString)
	}
	if len(queryResult[0].Series) == 0 {
		jsonData["status"]["success"] = false
		jsonData["status"]["errcode"] = "404" //not found
		jsonString, _ := json.Marshal(jsonData)
		return fmt.Sprintf("%s", jsonString)
	}
	jsonData["status"]["success"] = true
	jsonData["payload"] = make(map[string]interface{})

	for _, row := range queryResult[0].Series[0].Values {
		for index, columnName := range queryResult[0].Series[0].Columns {
			jsonData["payload"][outputFieldName[columnName]] = row[index]
		}
	}
	timeStamp, _ := time.Parse(time.RFC3339, jsonData["payload"]["timestamp"].(string))
	jsonData["payload"]["timestamp"] = timeStamp.UnixNano() / int64(time.Millisecond)
	jsonString, _ := json.Marshal(jsonData)
	return fmt.Sprintf("%s", jsonString)
}
