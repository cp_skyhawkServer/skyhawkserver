package router

import (
	"bytes"
	"fmt"
	"time"

	"../Device"
	"../common"
	"github.com/surgemq/message"
)

func mqttRouter(msg *message.PublishMessage) error {
	jsonData := common.RequestFormatter(bytes.Trim(msg.Payload(), "\x00"))
	switch jsonData["dataType"] {
	case "DeviceStatus":
		go print("\n[+] Received DeviceStatus from device.")
		Device.LogPhysicsInfo(jsonData)
	case "Greeting":
		go print("\n[+] Initializing new trip")
		Device.GenerateTripID(jsonData)
	default:
		fmt.Printf("[!] Unknown message from device:\"%s\"", msg.Payload())
	}
	return nil
}

func onComplete(msg, ack message.Message, err error) error {
	common.CheckErr(err)
	return err
}

// MQTT : handle with MQTT package that sent from device
func MQTT() {
	for true {
		common.CheckErr(common.ConnectToMQTTServer())

		submsg := message.NewSubscribeMessage()
		submsg.AddTopic([]byte("skyhawkPhase1"), 2)

		common.CheckErr(common.MqttListener.Subscribe(submsg, onComplete, mqttRouter))

		println("Connected.")
		time.Sleep(55 * time.Second)
		print("Reconnecting...")
	}
}
