package router

import (
	"../Client"
	"github.com/gin-gonic/gin"
)

// HTTPAPI : Backend API Handler
func HTTPAPI(r *gin.Engine) {
	httpAPI := r.Group("api/v1")
	{
		// HTTP API Routing rule
		httpAPI.POST("/login", Client.CheckLogin)
		httpAPI.GET("/logout", Client.Logout)
		httpAPI.GET("/authStatus", Client.AuthStatus)
		httpAPI.GET("/UserProfile", Client.UserProfile)
		httpAPI.GET("/allTripID", Client.GetAllTripID)
		httpAPI.GET("/deviceLog/physical/:flightID", Client.ViewDevicePhysicalLog)
	}
}
